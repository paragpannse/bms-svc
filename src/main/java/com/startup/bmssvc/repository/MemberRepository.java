package com.startup.bmssvc.repository;

import java.util.List;
import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.startup.bmssvc.entity.Member;


@Repository
public interface MemberRepository extends JpaRepository<Member, UUID> {
	
	List<Member> findByOrgId(UUID orgId);
}

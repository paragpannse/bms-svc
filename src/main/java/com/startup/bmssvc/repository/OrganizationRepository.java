package com.startup.bmssvc.repository;

import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.startup.bmssvc.entity.Organization;

@Repository
public interface OrganizationRepository extends JpaRepository<Organization, UUID> {

}

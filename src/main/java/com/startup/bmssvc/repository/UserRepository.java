package com.startup.bmssvc.repository;

import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.startup.bmssvc.entity.User;

@Repository
interface UserRepository extends JpaRepository<User, UUID> {
	
	User findByOrgId(String orgId);
}

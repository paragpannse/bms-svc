package com.startup.bmssvc;

import java.util.TimeZone;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.env.Environment;

import springfox.documentation.swagger2.annotations.EnableSwagger2;


@SpringBootApplication
@EnableSwagger2
public class BmsSvcApplication {

	private static ConfigurableApplicationContext appContext;
	
	public static void main(String[] args) {
		appContext = SpringApplication.run(BmsSvcApplication.class, args);
		Environment appEnvironment = appContext.getEnvironment();
		String applicationTimeZone =  appEnvironment.getProperty("bms.time-zone");
		TimeZone.setDefault(TimeZone.getTimeZone(applicationTimeZone));
	}
}

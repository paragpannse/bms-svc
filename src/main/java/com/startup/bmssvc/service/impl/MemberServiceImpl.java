package com.startup.bmssvc.service.impl;

import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.startup.bmssvc.dto.MemberDto;
import com.startup.bmssvc.entity.Member;
import com.startup.bmssvc.exception.NotFoundException;
import com.startup.bmssvc.repository.MemberRepository;
import com.startup.bmssvc.service.MemberService;

@Service
public class MemberServiceImpl extends BaseService implements MemberService {
	
	@Autowired
	private MemberRepository memberRepository;

	@Override
	public List<MemberDto> getAll(UUID orgId) {
		// TODO Auto-generated method stub
		List<Member> members = memberRepository.findByOrgId(orgId);
		return toMemberToList(members);
	}
	
	private List<MemberDto> toMemberToList(List<Member> members) {
		return members.stream().map(member -> modelMapper.map(member, MemberDto.class)).collect(Collectors.toList());
	}

	@Override
	public MemberDto save(MemberDto memberDto) {		
		Member member = modelMapper.map(memberDto, Member.class);
		member.setIsActive(Boolean.TRUE);
		memberRepository.save(member);
		return modelMapper.map(member, MemberDto.class);
	}

	@Override
	public MemberDto getOne(UUID memberId) {
		Optional<Member> optional = memberRepository.findById(memberId);
		if (!optional.isPresent()) {
			throw new NotFoundException("Invalid member Id provided. memberId : " + memberId);
		}
		return modelMapper.map(optional.get(), MemberDto.class);
	}

	@Override
	public MemberDto update(MemberDto memberDto) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean delete(UUID memberId) {
		if(memberRepository.existsById(memberId)){
			memberRepository.deleteById(memberId);
			return true;
		}else{
			throw new NotFoundException("Member not found with given Id " + memberId);
		}
	}
}

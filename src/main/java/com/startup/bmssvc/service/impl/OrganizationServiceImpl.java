package com.startup.bmssvc.service.impl;

import java.util.Optional;
import java.util.UUID;

import org.hibernate.service.spi.OptionallyManageable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.expression.spel.support.ReflectivePropertyAccessor.OptimalPropertyAccessor;
import org.springframework.stereotype.Service;

import com.startup.bmssvc.dto.OrganizationDto;
import com.startup.bmssvc.dto.RegistrationDto;
import com.startup.bmssvc.dto.RegistrationPostDto;
import com.startup.bmssvc.entity.Organization;
import com.startup.bmssvc.exception.NotFoundException;
import com.startup.bmssvc.repository.OrganizationRepository;
import com.startup.bmssvc.service.OrganizationService;

@Service
public class OrganizationServiceImpl extends BaseService implements OrganizationService {
	
	@Autowired
	private OrganizationRepository organizationRepository;

	@Override
	public RegistrationDto save(RegistrationPostDto registrationPostDto) {
		
		Organization organization = modelMapper.map(registrationPostDto, Organization.class);
		organization.setIsActive(Boolean.FALSE);
		organizationRepository.save(organization);
		return modelMapper.map(organization, RegistrationDto.class);
		// send notification to user on register email id and mobile number
	}

	@Override
	public OrganizationDto getOne(UUID orgId) {
		Organization organization = organizationRepository.getOne(orgId);
		return modelMapper.map(organization, OrganizationDto.class);
	}

	@Override
	public boolean delete(UUID orgId) {
		// TODO Auto-generated method stub
		if (organizationRepository.existsById(orgId))
		{
			organizationRepository.deleteById(orgId);
			return Boolean.TRUE;
		} else {
			throw new NotFoundException("Invalid input orgId : " + orgId);
		}
	}
	
	@Override
	public boolean deActive(UUID orgId) {
		// TODO Auto-generated method stub
		if (organizationRepository.existsById(orgId))
		{
			Organization organization = organizationRepository.getOne(orgId);
			organization.setIsActive(Boolean.FALSE);
			return Boolean.TRUE;
		} else {
			throw new NotFoundException("Invalid input orgId : " + orgId);
		}
	}
}

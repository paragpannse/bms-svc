package com.startup.bmssvc.service;

import java.util.UUID;

import com.startup.bmssvc.dto.OrganizationDto;
import com.startup.bmssvc.dto.RegistrationDto;
import com.startup.bmssvc.dto.RegistrationPostDto;

public interface OrganizationService {
	
	RegistrationDto save(RegistrationPostDto registrationDto);
	
	OrganizationDto getOne(UUID orgId);
		
	boolean delete(UUID orgId);
	
	boolean deActive(UUID orgId);
}

package com.startup.bmssvc.service;

import java.util.UUID;

import com.startup.bmssvc.dto.MemberDto;
import com.startup.bmssvc.dto.UserDto;

public interface UserService {
	
	UserDto getAll(UUID orgId);
	
	UserDto save(MemberDto userDto);
	
	UserDto getOne(String userId);
	
	UserDto update (UserDto userDto);
	
	UserDto delete(String userId);

}

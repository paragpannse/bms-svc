package com.startup.bmssvc.service;

import java.util.List;
import java.util.UUID;

import com.startup.bmssvc.dto.MemberDto;

public interface MemberService {
	
	List<MemberDto> getAll(UUID orgId);
	
	MemberDto save(MemberDto memberDto);
	
	MemberDto getOne(UUID memberId);
	
	MemberDto update (MemberDto memberDto);
	
	boolean delete(UUID memberId);

}

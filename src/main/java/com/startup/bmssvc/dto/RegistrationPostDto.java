package com.startup.bmssvc.dto;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;

public class RegistrationPostDto {
	
	@NotEmpty
	@Min(value = 2)
	@Max(value = 100)
	private String OrgName;
	
	@NotEmpty
	@Min(value = 3)
	@Max (value = 5)
	private String orgShortName;
	
	@NotEmpty
	@Min(value = 2)
	@Max(value = 200)
	private String address;
	
	@NotEmpty
	@Min(value = 2)
	@Max(value = 50)
	private String emailId;
	
	@NotEmpty
	@Min(value = 2)
	@Max(value = 12)
	private String contactNumber;
	
	@NotEmpty
	@Min(value = 2)
	@Max(value = 15)
	private String officeNumber;
	
	@NotEmpty
	@Min(value = 2)
	@Max(value = 2000)
	private String logo;

	public String getOrgName() {
		return OrgName;
	}

	public void setOrgName(String orgName) {
		OrgName = orgName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getContactNumber() {
		return contactNumber;
	}

	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}

	public String getOfficeNumber() {
		return officeNumber;
	}

	public void setOfficeNumber(String officeNumber) {
		this.officeNumber = officeNumber;
	}

	public String getLogo() {
		return logo;
	}

	public void setLogo(String logo) {
		this.logo = logo;
	}

}

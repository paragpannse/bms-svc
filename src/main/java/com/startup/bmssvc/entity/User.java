package com.startup.bmssvc.entity;

import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "USER")
public class User extends BaseEntity {
	
	@Column(name = "NAME")
	private String name;
	
	@Column(name = "ORG_ID")
	private UUID orgId;
	
	@Column(name = "CONTACT_NUMBER")
	private String contactNumber;
	
	@Column(name = "PASSWORD")
	private String password;
	
	// @Column(name = "role")
	// private Role role;
	
	
}

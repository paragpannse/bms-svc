package com.startup.bmssvc.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 * @author Admin
 *
 */
@Table(name = "ORGANIZATION")
@Entity
public class Organization extends BaseEntity {
	
	@Column(name = "ORG_NAME")
	private String OrgName;
	
	@Column(name = "ORG_SHORT_NAME")
	private String orgShortName;
	
	@Column(name = "ADDRESS")
	private String address;
	
	@Column(name = "EMAIL_ID")
	private String emailId;
	
	@Column(name = "CONTACT_NUMBER")
	private String contactNumber;
	
	@Column(name = "OFFICE_NUMBER")
	private String officeNumber;
	
	@Column(name = "LOGO")
	private String logo;
	
	@Column(name = "IS_VERIFIED")
	private Boolean isVerified;
	
//	@ManyToOne
//	@JoinColumn(name = "id")
	@Transient
	private Organization organization;

	public String getOrgName() {
		return OrgName;
	}

	public void setOrgName(String orgName) {
		OrgName = orgName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getContactNumber() {
		return contactNumber;
	}

	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}

	public String getOfficeNumber() {
		return officeNumber;
	}

	public void setOfficeNumber(String officeNumber) {
		this.officeNumber = officeNumber;
	}

	public String getLogo() {
		return logo;
	}

	public void setLogo(String logo) {
		this.logo = logo;
	}

	public Boolean getIsVerified() {
		return isVerified;
	}

	public void setIsVerified(Boolean isVerified) {
		this.isVerified = isVerified;
	}

	public Organization getOrganization() {
		return organization;
	}

	public void setOrganization(Organization organization) {
		this.organization = organization;
	}
}

package com.startup.bmssvc.entity;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "MEMBER")
public class Member extends BaseEntity {
	
	// Intername use only and unique
	@Column(name = "MEMBER_ID")
	private String memberId;
	
	@Column(name = "NAME")
	private String name;
	
	@Column(name = "CONTACT_NUMBER")
	private String contactNumber;
	
	@Column(name = "AdDDRESS")
	private String address;
	
	@Column(name = "ORG_ID")
	private String orgId;
	
	@Column(name = "ID_TYPE")
	private String IdProofType;
	
	@Column(name = "Id_PROOF")
	private String IdProofNumber;
	
	@Column(name = "DATE_OF_BIRTH")
	private Timestamp dateOfBirth;
	
	@Column(name = "photo")
	private String photo;
	
}

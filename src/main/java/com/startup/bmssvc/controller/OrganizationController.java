package com.startup.bmssvc.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.startup.bmssvc.dto.OrganizationDto;
import com.startup.bmssvc.service.OrganizationService;

@RestController
public class OrganizationController  extends BMSBaseController {

	@Autowired
	private OrganizationService organizationService;
	
	@GetMapping("/")
	public String welcome(){
		return "Welcome to Application";
	}
	
	@GetMapping("/org/{orgId}")
	public ResponseEntity<OrganizationDto> getOrganization(@PathVariable("orgId") String orgId) {
		OrganizationDto organization = organizationService.getOne(toUuid(orgId));
		return new ResponseEntity<>(organization, HttpStatus.OK);
	}
	
	@DeleteMapping("/org/{orgId}")
	public void deleteOrganization(@PathVariable("orgId") String orgId) {
		organizationService.delete(toUuid(orgId));
	}
	 
}

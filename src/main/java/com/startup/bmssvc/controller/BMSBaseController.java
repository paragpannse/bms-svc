package com.startup.bmssvc.controller;

import java.util.UUID;

public class BMSBaseController {
    
	
	protected UUID toUuid(String uuid){
		return UUID.fromString(uuid);
	}
}

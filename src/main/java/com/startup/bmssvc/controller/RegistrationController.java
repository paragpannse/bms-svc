package com.startup.bmssvc.controller;

import javax.imageio.spi.RegisterableService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.startup.bmssvc.dto.RegistrationPostDto;
import com.startup.bmssvc.service.OrganizationService;

import io.swagger.annotations.Api;

@RestController(value = "/registration")
@Api(description = "Registration conroller")
public class RegistrationController {
	
	@Autowired
	private OrganizationService organizationService;
	
	@PostMapping
	public ResponseEntity<Object> registerOrganization(
			@RequestBody RegistrationPostDto registrationDto){
		organizationService.save(registrationDto);
		return new ResponseEntity<>(HttpStatus.OK);
	}

}
